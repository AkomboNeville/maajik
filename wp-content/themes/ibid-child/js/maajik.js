/*
 Project name:       ibid
 Project author:     ModelTheme
 File name:          Maajik JS
*/

(function ($) {
    'use strict';

    $(document).ready(function() {

        // hide show modal content
 
        jQuery("#nav-menu-login #nav-register-modal").on("click",function(){                           
            jQuery("#login-modal-content").hide();
            jQuery("#signup-modal-content").show();   
        });
        jQuery("#nav-menu-login #modal-log-in").on("click",function(){                           
            jQuery("#login-modal-content").show();
            jQuery("#signup-modal-content").hide();   
        });

        // loader function
        function loader (){
            jQuery(document).ajaxStart(function(){
                $("#wait").css({'display':'block',
                'z-index':'10000000',
                'bottom':'50',
                'margin':'auto'});
                });
                jQuery(document).ajaxComplete(function(){
                    $("#wait").css("display", "none");
            }); 
        }

        // login
        
        jQuery("#modal-log-in #loginform").on('submit',function(e){
            e.preventDefault();
                loader();
                   
               let log = jQuery('input[name = "log"]').val();
               let pwd = jQuery('input[name = "pwd"]').val();
               let redirect_to =jQuery('input[name = "redirect_to"]').val();
                jQuery.ajax({
                    type: "POST",
                    dataType: "json",
                    url: maAjax.ajaxurl,
                    data: {
                        log:log,
                        pwd:pwd,
                        action:"user_login_verification",
                        'security': $('#loginform #security').val()
                    },
                    success: function (data) {
                        console.log(data)    
    
                    
                        if(data.type == "error") {
                            jQuery("#login-error").html(data.error);
                            
                         }
                         else{
                            e.currentTarget.submit();
                            // window.location.replace(redirect_to);
                         }

                    }
          
                });

         });

        //  registration
         jQuery("#modal-log-in form.woocommerce-form-register button").on('click',function(e){
             e.preventDefault();
             loader();
                     
            jQuery(document).ajaxStart(function(){
                $("#wait").css({'display':'block',
                'z-index':'10000000',
                'bottom':'50',
                'margin':'auto'});
                });
                jQuery(document).ajaxComplete(function(){
                    $("#wait").css("display", "none");
            }); 
            
             let billing_first_name = jQuery('input[name = "billing_first_name"]').val();
             let billing_last_name = jQuery('input[name = "billing_last_name"]').val();
             let email = jQuery('input[name = "email"]').val();
             let password = jQuery('input[name = "password"]').val();
             let username = jQuery('input[name = "username"]').val();
             let redirect_to =jQuery('input[name = "redirect_to"]').val();

                jQuery.ajax({
                    type: "POST",
                    dataType: "json",
                    url: maAjax.ajaxurl,
                    data: {
                        billing_first_name:billing_first_name,
                        billing_last_name:billing_last_name,
                        action:"user_registration",
                        email:email,
                        password:password,
                        username:username
                        
                    },
                    success: function (data) {
                        if(data.type =="errors") {
                            jQuery("#registration-error").html(data.error);
                         }

                         else{
                            jQuery("#registration-error").css('color','green');
                            jQuery("#registration-error").html(data.success);

                              setTimeout(function(){
                                window.location.replace(redirect_to);
                                
                              }, 3000);
                          
                         }
                    }
                            
                });
            
                          

        });

        // slider
        /*Begin: Auctions slider*/
        jQuery(".auctions_slider").owlCarousel({
            navigation      : false, // Show next and prev buttons
            // pagination      : true,
            autoPlay        : 3000,
            singleItem      : false,
            rewindNav       :false
        });
        /*End: Auctions slider*/

    });
    

} (jQuery) )