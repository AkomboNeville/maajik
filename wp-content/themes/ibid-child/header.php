<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<?php 
#Redux global variable
global $ibid_redux;

?>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) { ?>
        <link rel="shortcut icon" href="<?php echo esc_url(ibid_redux('ibid_favicon', 'url')); ?>">
    <?php } ?>
    <?php wp_head(); ?>
</head>



<body <?php body_class(); ?>>
    <?php

    if (!in_array('login-register-page', get_body_class())) { ?>
        <div class="modeltheme-modal" id="modal-log-in">
            <div class="modeltheme-content" id="login-modal-content">
                <h3 class="relative">
                    <?php echo esc_html__('Login to Your Account', 'ibid'); ?>
                </h3>
                <div class="modal-content row">
                    <div class="col-md-12">
                        
                        <form name="loginform" id="loginform" action="<?php echo wp_login_url(); ?>" method="post">
                            <p id="login-error"></p>
            
                            <p class="login-username">
                                <label for="user_login"><?php echo esc_html__('Username or Email Address','ibid'); ?></label>
                                <i class="fa fa-user-o" aria-hidden="true"></i>
                                <input type="text" name="log" id="user_login" class="input" value="" size="20" placeholder="<?php echo esc_attr__('Username','ibid'); ?>" required>
                            </p>
                            <i id="name-error" style="color:red"></i>
                            <p class="login-password">
                                <label for="user_pass"><?php echo esc_html__('Password','ibid'); ?></label>
                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                <input type="password" name="pwd" id="user_pass" class="input" value="" size="20" placeholder="<?php echo esc_attr__('Password','ibid'); ?>" required>
                            </p>
                            <i id="pwd-error" style="color:red"></i>
                            
                            <p class="login-remember">
                                <label>
                                    <input name="rememberme" type="checkbox" id="rememberme" value="forever">
                                    <?php echo esc_html__('Remember Me','ibid'); ?>
                                </label>
                            </p>
                            <div class="row-buttons">
                                    <?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>
                                    <p class="login-submit">
                                        <input type="submit" name="wp-submit" id="wp-submit" class="button button-primary" value="<?php echo esc_attr__('Log In','ibid'); ?>">
                                        <input type="hidden" name="redirect_to" value="<?php echo get_site_url(); ?>">
                                    </p>
                                    <?php if (  get_option('users_can_register')) { ?>
                                        <p class="btn-register-p">
                                            <a class="btn btn-register" id="register-modal"><?php echo esc_html__('Register','ibid'); ?></a>
                                        </p>
                                    <?php } else { ?>
                                        <p class="um-notice err text-center"><?php echo esc_html__('Registration is currently disabled','ibid'); ?></p>
                                    <?php } ?>
                                    <p class="woocommerce-LostPassword lost_password">
                                        <a href="<?php echo esc_url(wp_lostpassword_url()); ?>"><?php echo esc_html__('Lost your password?','ibid'); ?></a>
                                    </p>
                            </div>

                            <div class="wp_login_error">
                                <?php if( isset( $_GET['login'] ) && $_GET['login'] == 'failed' ) { ?>
                                    <p>The password you entered is incorrect, Please try again.</p>
                                <?php } 
                                else if( isset( $_GET['login'] ) && $_GET['login'] == 'empty' ) { ?>
                                    <p>Please enter both username and password.</p>
                                <?php } ?>
                            </div> 
                            
                        </form>
                        <!--?php if (function_exists('YITH_WC_Social_Login')) { ?>
                            <div class="separator-modal"><!?php echo esc_html__('OR ','ibid'); ?></div>
                            <!?php echo do_shortcode("[yith_wc_social_login]"); ?>
                        <!?php } ?-->
                    </div>
                </div>
            </div>

             <!-- added woocommerce class  -->

            <div class="modeltheme-content woocommerce" id="signup-modal-content">
                <h3 class="relative">
                    <?php echo esc_html__('Personal Details','ibid'); ?>
                </h3>
                <div class="modal-content row">
                    <div class="col-md-12">
                        <?php if ( class_exists( 'WooCommerce' ) ) { ?>
                            <?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) { ?>
                                <div class="u-column2 col-2">
                                    
                                    <form method="post" class="woocommerce-form woocommerce-form-register register">
                                        <!-- < -->
                                        <?php do_action( 'woocommerce_register_form_start' ); ?>
                                        <p id="registration-error"></p>
                                        <?php if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>
                                            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                                <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="reg_username" autocomplete="username" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" placeholder="<?php esc_attr_e( 'Username', 'ibid' ); ?>" required="required" minlength="5"/>

                                            </p>
                                            <p id="username_exist" class="form-errors"></p>
                                        <?php endif; ?>
                                        <p class="form-row form-row-first">
                                            <input type="text" class="input-text" name="billing_first_name" id="billing_first_name" value="<?php if (!empty($_POST['billing_first_name'])) esc_attr_e($_POST['billing_first_name'], 'ibid'); ?>" placeholder="<?php esc_attr_e( 'First name', 'ibid' ); ?>" required="required"/>
                                        </p>
                                        <p class="form-row form-row-last">
                                            <input type="text" class="input-text" name="billing_last_name" id="billing_last_name" value="<?php if (!empty($_POST['billing_last_name'])) esc_attr_e($_POST['billing_last_name'], 'ibid'); ?>" placeholder="<?php esc_attr_e( 'Last name', 'ibid' ); ?>" required="required" />
                                        </p>
                                        <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                            <input type="email" class="woocommerce-Input woocommerce-Input--text input-text" name="email" id="reg_email" autocomplete="email" value="<?php echo ( ! empty( $_POST['email'] ) ) ? esc_attr( wp_unslash( $_POST['email'] ) ) : ''; ?>" placeholder="<?php esc_attr_e( 'Email address', 'ibid' ); ?>"  required="required"/>
                                        </p>
                                        <p id="email_exist" class="form-errors"></p>
                                        <?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>
                                            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                                <input type="password" class="woocommerce-Input woocommerce-Input--text input-text" name="password" id="reg_password" autocomplete="new-password" placeholder="Password" required="required" minlength="8" />
                                            </p>
                                        <?php endif; ?>
                                        <?php do_action( 'woocommerce_register_form' ); ?>
                                        <p class="woocommerce-FormRow form-row">
                                            <?php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>
                                            <button type="submit" class="woocommerce-Button button woocommerce-form-register__submit" name="register" value="<?php esc_attr_e( 'Register', 'ibid' ); ?>"><?php esc_html_e( 'Register', 'ibid' ); ?></button>
                                            <!-- Back to login -->
                                            <a class="btn btn-login btn-login-p" id="login-modal"><?php echo esc_html__('Back to Login','ibid'); ?></a>
                                        </p>
                                        <?php //do_action( 'woocommerce_register_form_end' ); ?>
                                    </form>
                                    <!--?php if (function_exists('yith_ywsl_constructor')) { ?>
                                        <div class="separator-modal"><!?php echo esc_html__('OR ','ibid'); ?></div>
                                        <!?php echo do_shortcode("[yith_wc_social_login]"); ?>
                                    <!?php } ?-->
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>            
            </div>
        </div>
    <?php } ?>
    <div class="modeltheme-overlay"></div>
    <div id="wait" style="display:none;"><img src="<?php echo get_stylesheet_directory_uri().'/images/tenor.gif'?>" width="64" height="64" /><br>Loading..</div>

    <div id="page" class="hfeed site">
    <?php
        if (is_page()) {
            $mt_custom_header_options_status = get_post_meta( get_the_ID(), 'ibid_custom_header_options_status', true );
            $mt_header_custom_variant = get_post_meta( get_the_ID(), 'ibid_header_custom_variant', true );
            if (isset($mt_custom_header_options_status) AND $mt_custom_header_options_status == 'yes') {
                get_template_part( 'templates/header-template'.esc_html($mt_header_custom_variant) );
            }else{
                // DIFFERENT HEADER LAYOUT TEMPLATES
                if ($ibid_redux['header_layout'] == 'first_header') {
                    // Header Layout #1
                    get_template_part( 'templates/header-template1' );
                }elseif ($ibid_redux['header_layout'] == 'fifth_header') {
                    // Header Layout #5
                    get_template_part( 'templates/header-template5' );
                }else{
                    // if no header layout selected show header layout #1
                    get_template_part( 'templates/header-template1' );
                } 
            }
        }else{
            // DIFFERENT HEADER LAYOUT TEMPLATES
            if ($ibid_redux['header_layout'] == 'first_header') {
                // Header Layout #1
                get_template_part( 'templates/header-template1' );
            }elseif ($ibid_redux['header_layout'] == 'fifth_header') {
                // Header Layout #5
                get_template_part( 'templates/header-template5' );
            }else{
                // if no header layout selected show header layout #1
                get_template_part( 'templates/header-template1' );
            }
        }
    ?>