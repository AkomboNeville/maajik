<?php
	/**
	 * This should be in its own separate file.
	 */
	class WC_Product_Exchange extends WC_Product {

		public function __construct( $product ) {

			$this->product_type = 'exchange';

			parent::__construct( $product );

		}

	}

?>