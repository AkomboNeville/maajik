<?php
/**
 * Register the custom product type after init
 */
function register_exchange_product_type() {

    require_once get_stylesheet_directory() . '/inc/exchange/exchange-class.php';
}

add_action( 'init', 'register_exchange_product_type' );

/**
 * add 
 */

function add_exchange_product( $types ){

	// Key should be exactly the same as in the class product_type parameter
	$types[ 'exchange' ] = __( 'Exchange');

	return $types;

}
add_filter( 'product_type_selector', 'add_exchange_product' );

/**
 * load new product type class
 */
 
function woocommerce_product_class( $classname, $product_type ) {
    if ( $product_type == 'exchange' ) { 
        $classname = 'WC_Product_Exchange';
    }
    return $classname;
}
add_filter( 'woocommerce_product_class', 'woocommerce_product_class', 10, 2 );