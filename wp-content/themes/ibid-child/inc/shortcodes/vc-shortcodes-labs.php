<?php

$animations_list = array(
    'bounce' => 'bounce',
    'flash' => 'flash',
    'pulse' => 'pulse',
    'rubberBand' => 'rubberBand',
    'shake' => 'shake',
    'swing' => 'swing',
    'tada' => 'tada',
    'wobble' => 'wobble',
    'bounceIn' => 'bounceIn',
    'bounceInDown' => 'bounceInDown',
    'bounceInLeft' => 'bounceInLeft',
    'bounceInRight' => 'bounceInRight',
    'bounceInUp' => 'bounceInUp',
    'bounceOut' => 'bounceOut',
    'bounceOutDown' => 'bounceOutDown',
    'bounceOutLeft' => 'bounceOutLeft',
    'bounceOutRight' => 'bounceOutRight',
    'bounceOutUp' => 'bounceOutUp',
    'fadeIn' => 'fadeIn',
    'fadeInDown' => 'fadeInDown',
    'fadeInDownBig' => 'fadeInDownBig',
    'fadeInLeft' => 'fadeInLeft',
    'fadeInLeftBig' => 'fadeInLeftBig',
    'fadeInRight' => 'fadeInRight',
    'fadeInRightBig' => 'fadeInRightBig',
    'fadeInUp' => 'fadeInUp',
    'fadeInUpBig' => 'fadeInUpBig',
    'fadeOut' => 'fadeOut',
    'fadeOutDown' => 'fadeOutDown',
    'fadeOutDownBig' => 'fadeOutDownBig',
    'fadeOutLeft' => 'fadeOutLeft',
    'fadeOutLeftBig' => 'fadeOutLeftBig',
    'fadeOutRight' => 'fadeOutRight',
    'fadeOutRightBi' => 'fadeOutRightBig',
    'fadeOutUp' => 'fadeOutUp',
    'fadeOutUpBig' => 'fadeOutUpBig',
    'flip' => 'flip',
    'flipInX' => 'flipInX',
    'flipInY' => 'flipInY',
    'flipOutX' => 'flipOutX',
    'flipOutY' => 'flipOutY',
    'lightSpeedIn' => 'lightSpeedIn',
    'lightSpeedOut' => 'lightSpeedOut',
    'rotateIn' => 'rotateIn',
    'rotateInDownLe' => 'rotateInDownLeft',
    'rotateInDownRi' => 'rotateInDownRight',
    'rotateInUpLeft' => 'rotateInUpLeft',
    'rotateInUpRigh' => 'rotateInUpRight',
    'rotateOut' => 'rotateOut',
    'rotateOutDownL' => 'rotateOutDownLeft',
    'rotateOutDownR' => 'rotateOutDownRight',
    'rotateOutUpLef' => 'rotateOutUpLeft',
    'rotateOutUpRig' => 'rotateOutUpRight',
    'hinge' => 'hinge',
    'rollIn' => 'rollIn',
    'rollOut' => 'rollOut',
    'zoomIn' => 'zoomIn',
    'zoomInDown' => 'zoomInDown',
    'zoomInLeft' => 'zoomInLeft',
    'zoomInRight' => 'zoomInRight',
    'zoomInUp' => 'zoomInUp',
    'zoomOut' => 'zoomOut',
    'zoomOutDown' => 'zoomOutDown',
    'zoomOutLeft' => 'zoomOutLeft',
    'zoomOutRight' => 'zoomOutRight',
    'zoomOutUp' => 'zoomOutUp'
  );

  #auction

  vc_map( array(
    "name" => esc_attr__("iBid - Recent Auction Slider", 'ibid'),
    "base" => "recent-auction-slider",
    "category" => esc_attr__('ibid', 'ibid'),
    "icon" => "modeltheme_shortcode",
    "params" => array(
       array(
          "type" => "textfield",
          "holder" => "div",
          "class" => "",
          "heading" => esc_attr__("Number of products to show ", 'ibid'),
          "param_name" => "number_of_products_by_auction"
       ),
       array(
         "type" => "dropdown",
         "heading" => esc_attr__("Animation", 'ibid'),
         "param_name" => "animation",
         "std" => 'fadeInLeft',
         "holder" => "div",
         "class" => "",
         "value" => $animations_list
       )
    )
));

  #featured

  vc_map( array(
   "name" => esc_attr__("iBid - Featured Products Slider", 'ibid'),
   "base" => "featured-products-slider",
   "category" => esc_attr__('ibid', 'ibid'),
   "icon" => "modeltheme_shortcode",
   "params" => array(
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_attr__("Number of products to show ", 'ibid'),
         "param_name" => "number_of_products_by_auction"
      ),
      array(
        "type" => "dropdown",
        "heading" => esc_attr__("Animation", 'ibid'),
        "param_name" => "animation",
        "std" => 'fadeInLeft',
        "holder" => "div",
        "class" => "",
        "value" => $animations_list
      )
   )
));

#. Masonry banners
vc_map( array(
    "name" => esc_attr__("iBid - Masonry Banners Labs", 'ibid'),
    "base" => "shop-masonry-banners-labs",
    "category" => esc_attr__('ibid', 'ibid'),
    "icon" => "modeltheme_shortcode",
    "params" => array(
        
        array(
            "group" => "Settings",
            "type" => "attach_image",
            "holder" => "div",
            "class" => "",
            "heading" => esc_attr__("#1 Banner Image", 'ibid'),
            "param_name" => "banner_1_img",
            "value" => esc_attr__("#", 'ibid')
        ),
        array(
            "group" => "Settings",
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => esc_attr__("#1 Banner Title", 'ibid'),
            "param_name" => "banner_1_title",
            "value" => esc_attr__("Banner 1 ", 'ibid')
        ),
        array(
            "group" => "Settings",
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => esc_attr__("#1 Banner Subtitle", 'ibid'),
            "param_name" => "banner_1_count"
        ),
        array(
            "group" => "Settings",
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => esc_attr__("#1 Banner Button text",'ibid'),
            "param_name" => "banner_1_btn"

        ),
        array(
            "group" => "Settings",
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => esc_attr__("#1 Banner Link", 'ibid'),
            "param_name" => "banner_1_url",
            "value" => esc_attr__("#", 'ibid')
        ),
        array(
            "group" => "Settings",
            "type" => "attach_image",
            "holder" => "div",
            "class" => "",
            "heading" => esc_attr__("#2 Banner Image", 'ibid'),
            "param_name" => "banner_2_img",
            "value" => esc_attr__("#", 'ibid')
        ),
        array(
            "group" => "Settings",
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => esc_attr__("#2 Banner Title", 'ibid'),
            "param_name" => "banner_2_title",
            "value" => esc_attr__("Beds", 'ibid')
        ),
        array(
            "group" => "Settings",
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => esc_attr__("#2 Banner Subtitle", 'ibid'),
            "param_name" => "banner_2_count"
        ),
        array(
            "group" => "Settings",
            "type" => "textfield",
            "holder" =>"div",
            "class" => "",
            "heading" => esc_attr__("#2 Banner Button Text",'ibid'),
            "param_name" => "banner_2_btn"
        ),
        array(
            "group" => "Settings",
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => esc_attr__("#2 Banner Link", 'ibid'),
            "param_name" => "banner_2_url",
            "value" => esc_attr__("#", 'ibid')
        ),
        array(
            "group" => "Styling",
            "type" => "colorpicker",
            "holder" => "div",
            "class" => "",
            "heading" => esc_attr__("Default skin background color", 'ibid'),
            "param_name" => "default_skin_background_color"
        ),
        array(
            "group" => "Styling",
            "type" => "colorpicker",
            "holder" => "div",
            "class" => "",
            "heading" => esc_attr__("Dark skin background color", 'ibid'),
            "param_name" => "dark_skin_background_color"
        )
    )
));  

# Sale
vc_map( array(
    "name" => esc_attr__("iBid - Products On Sales", 'ibid'),
    "base" => "product-categories-with-xsthumbnails",
    "category" => esc_attr__('ibid', 'ibid'),
    "icon" => "modeltheme_shortcode",
    "params" => array(
 
       array(
          "group" => "Settings",
          "type" => "textfield",
          "holder" => "div",
          "class" => "",
          "heading" => esc_attr__("Number of products to show for each category", 'ibid'),
          "param_name" => "number_of_products_by_category"
       ),
       array(
          "group" => "Settings",
          "type" => "dropdown",
          "holder" => "div",
          "class" => "",
          "heading" => esc_attr__("Show categories without products?", 'ibid'),
          "param_name" => "hide_empty",
          "std" => 'true',
          "value" => array(
             'Yes'     => 'true',
             'No'        => 'false'
          ),
       ),
       array(
          "group" => "Settings",
          "type" => "dropdown",
          "holder" => "div",
          "class" => "",
          "heading" => esc_attr__("Products per column", 'ibid'),
          "param_name" => "number_of_columns",
          "std" => '2',
          "value" => array(
             '2'        => '2',
             '3'        => '3',
             '4'        => '4'
          ),
       ),
       array(
                "group" => "Styling",
                "type" => "colorpicker",
                "class" => "",
                "heading" => esc_attr__( "Background Banner Color 1", 'modeltheme' ),
                "param_name" => "overlay_color1",
                "value" => "", //Default color
                "description" => esc_attr__( "Choose banner color", 'modeltheme' )
       ),
       array(
                "group" => "Styling",
                "type" => "colorpicker",
                "class" => "",
                "heading" => esc_attr__( "Background Banner Color 2", 'modeltheme' ),
                "param_name" => "overlay_color2",
                "value" => "", //Default color
                "description" => esc_attr__( "Choose banner color", 'modeltheme' )
       ),
          array(
             "type" => "attach_image",
                "group" => "Styling",
             "holder" => "div",
             "class" => "",
             "heading" => esc_attr__( "Background Image (Optional)", 'ibid' ),
             "description" => esc_attr__("If this option is empty, the colors from colorpickers will be applied.", 'ibid'),
             "param_name" => "bg_image",
          ),
    )
));

# On Exchange
vc_map( array(
    "name" => esc_attr__("iBid - Products On Exchange", 'ibid'),
    "base" => "exchange-product-categories-with-xsthumbnails",
    "category" => esc_attr__('ibid', 'ibid'),
    "icon" => "modeltheme_shortcode",
    "params" => array(
 
       array(
          "group" => "Settings",
          "type" => "textfield",
          "holder" => "div",
          "class" => "",
          "heading" => esc_attr__("Number of products to show for each category", 'ibid'),
          "param_name" => "number_of_products_by_category"
       ),
       array(
          "group" => "Settings",
          "type" => "dropdown",
          "holder" => "div",
          "class" => "",
          "heading" => esc_attr__("Show categories without products?", 'ibid'),
          "param_name" => "hide_empty",
          "std" => 'true',
          "value" => array(
             'Yes'     => 'true',
             'No'        => 'false'
          ),
       ),
       array(
          "group" => "Settings",
          "type" => "dropdown",
          "holder" => "div",
          "class" => "",
          "heading" => esc_attr__("Products per column", 'ibid'),
          "param_name" => "number_of_columns",
          "std" => '2',
          "value" => array(
             '2'        => '2',
             '3'        => '3',
             '4'        => '4'
          ),
       ),
       array(
                "group" => "Styling",
                "type" => "colorpicker",
                "class" => "",
                "heading" => esc_attr__( "Background Banner Color 1", 'modeltheme' ),
                "param_name" => "overlay_color1",
                "value" => "", //Default color
                "description" => esc_attr__( "Choose banner color", 'modeltheme' )
       ),
       array(
                "group" => "Styling",
                "type" => "colorpicker",
                "class" => "",
                "heading" => esc_attr__( "Background Banner Color 2", 'modeltheme' ),
                "param_name" => "overlay_color2",
                "value" => "", //Default color
                "description" => esc_attr__( "Choose banner color", 'modeltheme' )
       ),
          array(
             "type" => "attach_image",
                "group" => "Styling",
             "holder" => "div",
             "class" => "",
             "heading" => esc_attr__( "Background Image (Optional)", 'ibid' ),
             "description" => esc_attr__("If this option is empty, the colors from colorpickers will be applied.", 'ibid'),
             "param_name" => "bg_image",
          ),
    )
));

# Recent Exchange
vc_map( array(
   "name" => esc_attr__("iBid - Recent Exchange ", 'ibid'),
   "base" => "recent_exchange_labs",
   "category" => esc_attr__('ibid', 'ibid'),
   "icon" => "modeltheme_shortcode",
));