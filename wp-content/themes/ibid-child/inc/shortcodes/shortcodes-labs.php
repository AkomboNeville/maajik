<?php

 /*---------------------------------------------*/
/*--- Masonry Banners ---*/
/*---------------------------------------------*/
function ibid_shop_masonry_banners_shortcode_labs( $params, $content ) {
    extract( shortcode_atts( 
        array(
            'default_skin_background_color'      => '',
            'dark_skin_background_color'         => '',
            'banner_1_img'                       => '',
            'banner_1_title'                     => '',
            'banner_1_count'                     => '',
            'banner_1_url'                       => '',
            'banner_2_img'                       => '',
            'banner_2_title'                     => '',
            'banner_2_count'                     => '',
            'banner_2_url'                       => '',
            'banner_1_btn'                       => '',
            'banner_2_btn'                     => '',
        ), $params ) );
        

    
    
    $shortcode_content = '';


    $shortcode_content .= '<div class="masonry_banners banners_column">';

        $img1 = wp_get_attachment_image_src($banner_1_img, "large");
        $img2 = wp_get_attachment_image_src($banner_2_img, "large");

        if (isset($banner_1_btn) && !empty($banner_1_btn)) {
            $banner_1_btn = $banner_1_btn;
        }else{
            $banner_1_btn = 'SOURCE NOW';
        }

        if (isset($banner_2_btn) && !empty($banner_2_btn)) {
            $banner_2_btn = $banner_2_btn;
        }else{
            $banner_2_btn = 'SOURCE NOW';
        }

        $shortcode_content .= '<div class="vc_col-md-6">';
            #IMG #1
            if (isset($img1) && !empty($img1)) {
                $shortcode_content .= '<div class="masonry_banner default-skin" style=" background-color: '.$default_skin_background_color.'!important;">';
                    $shortcode_content .= '<a href="'.$banner_1_url.'" class="relative">';
                        $shortcode_content .= '<img src="'.$img1[0].'" alt="'.$banner_1_title.'" />';
                        $shortcode_content .= '<div class="masonry_holder">';
                            $shortcode_content .= '<h3 class="category_name">'.$banner_1_title.'</h3>';
                             $shortcode_content .= '<p class="category_count">'.$banner_1_count.'</p>';
                            $shortcode_content .= '<span class="read-more">'.esc_attr__($banner_1_btn, 'ibid').'</span>';
                        $shortcode_content .= '</div>';
                    $shortcode_content .= '</a>';
                $shortcode_content .= '</div>';
            }

        $shortcode_content .= '</div>';

        $shortcode_content .= '<div class="vc_col-md-6">';
            #IMG #2
            if (isset($img2) && !empty($img2)) {
                $shortcode_content .= '<div class="masonry_banner dark-skin" style="background-color: '.$dark_skin_background_color.'!important;">';
                    $shortcode_content .= '<a href="'.$banner_2_url.'" class="relative">';
                        $shortcode_content .= '<img src="'.$img2[0].'" alt="'.$banner_2_title.'" />';
                        $shortcode_content .= '<div class="masonry_holder">';
                            $shortcode_content .= '<h3 class="category_name">'.$banner_2_title.'</h3>';
                                $shortcode_content .= '<p class="category_count">'.$banner_2_count.'</p>';
                            $shortcode_content .= '<span class="read-more">'.esc_attr__($banner_2_btn, 'ibid').'</span>';
                        $shortcode_content .= '</div>';
                    $shortcode_content .= '</a>';
                $shortcode_content .= '</div>';
            }

        $shortcode_content .= '</div>';
    $shortcode_content .= '</div>';

    return $shortcode_content;
}
add_shortcode('shop-masonry-banners-labs', 'ibid_shop_masonry_banners_shortcode_labs');


 
 #2.products on sale

 function ibid_product_onsale_with_xsthumbnails_shortcode( $params, $content ) {
    extract( shortcode_atts( 
        array(
            'number'                               => '',
            'number_of_products_by_category'       => '',
            'number_of_columns'                    => '',
            'button_text'                    => 'cick here to view',
            'products_label_text'                    => '',
            'category'                             => '',
            'overlay_color1'                       => '',
            'overlay_color2'                       => '',
            'bg_image'                       => '',
            'hide_empty'                           => '',
            'sale_text'                        =>''
        ), $params ) );

    if (isset($bg_image) && !empty($bg_image)) {
        $bg_image = wp_get_attachment_image_src($bg_image, "full");
    }

    $category_style_bg = '';
    if (isset($bg_image) && !empty($bg_image)) {
        $category_style_bg .= 'background: url('.$bg_image[0].') no-repeat center center;';
    }else{
        $category_style_bg .= 'background: radial-gradient('.$overlay_color1.','.$overlay_color2.');';
    }

    // if ($button_text) {
    // 	$button_text_value = $button_text;
    // }else{
    // 	$button_text_value = 'View All Items';
    // }

    if ($products_label_text) {
    	$products_label_text_value = $products_label_text;
    }else{
    	$products_label_text_value = 'Products';
    }
    if(!$sale_text){
        $sale_text = 'ON SALE';
    }
// '.do_shortcode('[sale_products columns="'.$number_of_columns.'" per_page="'.$number_of_products_by_category.'"]').'

    // $cat = get_term_by('slug', $category, 'product_cat');
    $sales = wc_get_product_ids_on_sale();

    $shortcode_content = '';
    $shortcode_content .= '<div class="woocommerce_categories2">';
       
        $shortcode_content .= '<div class="products_category row">';
            $shortcode_content .= '<div class="category item col-md-3" >';
                $shortcode_content .= '<div style="'.$category_style_bg.'" class="category-wrapper">';
                    $shortcode_content .= '<a class="#categoryid">';
                        $shortcode_content .= '<span class="cat-name">'.$sale_text.'</span>';                    
                    $shortcode_content .= '</a>';
                    $shortcode_content .= '<br>'; 

                    $shortcode_content .= '<span class="cat-count"><strong>'.count($sales).'</strong> '.esc_html($products_label_text_value).'</span>';
                    $shortcode_content .= '<br>';
                    $shortcode_content .= '<div class="category-button">';
                    //    $shortcode_content .= '<a href="  '.get_site_url().'/product-category/'.$category.'  " class="button" title="View more" ><span>'.$button_text_value.'</span></a>';
                    $shortcode_content .= '</div>';
                $shortcode_content .= '</div>';    
            $shortcode_content .= '</div>';
                        $shortcode_content .= '<div id="categoryid" class=" col-md-9 products_by_categories ">'.do_shortcode('[sale_products columns="'.$number_of_columns.'" per_page="'.$number_of_products_by_category.'"]').'
                        </div>';
        $shortcode_content .= '</div>';
    $shortcode_content .= '</div>';

    wp_reset_postdata();

    return $shortcode_content;
}
add_shortcode('product-categories-with-xsthumbnails', 'ibid_product_onsale_with_xsthumbnails_shortcode');


#auction

function ibid_auction_shortcode($params, $content) {
    extract( shortcode_atts( 
        array(
            'number_of_products_by_auction'=>'',
            'animation'=>'',
        ), $params ) );
        $args_recenposts = array(
                'posts_per_page'   => $number_of_products_by_auction,
                'orderby'          => 'post_date',
                'order'            => 'DESC',
                'post_type'        => 'product',
                'post_status'      => 'publish' 
                );
        $recentposts = get_posts($args_recenposts);
        $content  = "";
        $content .= '<div id="categoryid_'.$animation.'" class=" col-md-12 products_by_categories '.$animation.'">';

            // $content .= '<div class="testimonials_slider owl-carousel owl-theme animateIn" data-animate="'.$animation.'">';
                $content .= do_shortcode('[recent_auctions_labs per_page="'.$number_of_products_by_auction.'" columns="1" orderby="date" order="desc"]');
        // $shortcode_content .= '<div id="categoryid_'.$cat->term_id.'" class=" col-md-12 products_by_categories '.$cat->name.'">'.do_shortcode('[product_category columns="'.$number_of_columns.'" per_page="'.$number_of_products_by_category.'" category="'.$category.'"]').'</div>';
            // $content .= '</div>';
        $content .= '</div>';
        return $content;
}
add_shortcode('recent-auction-slider', 'ibid_auction_shortcode');

 	/**
	 * recent_auctions shortcode
	 *
	 * @access public
	 * @param array $atts
	 * @return string
	 */
    function recent_auctions_labs( $atts ) {

		global $woocommerce_loop, $woocommerce;

		extract(shortcode_atts(array(
			'per_page' 	=> '12',
			'columns' 	=> '4',
			'orderby' => 'date',
			'order' => 'desc'
		), $atts));

		$meta_query = $woocommerce->query->get_meta_query();

		$args = array(
			'post_type'	=> 'product',
			'post_status' => 'publish',
			'ignore_sticky_posts'	=> 1,
			'posts_per_page' => $per_page,
			'orderby' => $orderby,
			'order' => $order,
			'meta_query' => $meta_query,
			'tax_query' => array(array('taxonomy' => 'product_type' , 'field' => 'slug', 'terms' => 'auction')),
			'auction_arhive' => TRUE
		);

		ob_start();

        $products = new WP_Query( $args );

		$woocommerce_loop['columns'] = $columns;

		if ( $products->have_posts() ) : ?>

			<?php woocommerce_product_loop_start(); ?>
			<div class="auctions_slider owl-carousel owl-theme animateIn" data-animate="<?=$animation ?>">

				<?php while ( $products->have_posts() ) : $products->the_post(); ?>

					<?php wc_get_template_part( 'content', 'product' ); ?>

				<?php endwhile; // end of the loop. ?>
			</div>

			<?php woocommerce_product_loop_end(); ?>
	   <?php else : ?>
            <?php wc_get_template( 'loop/no-products-found.php' ); ?>

		<?php endif;

		wp_reset_postdata();

		return '<div class="woocommerce">' . ob_get_clean() . '</div>';
    }
    add_shortcode( 'recent_auctions_labs', 'recent_auctions_labs' );


#featured product slider

function ibid_featured_shortcode($params, $content) {
    extract( shortcode_atts( 
        array(
            'number_of_products_by_auction'=>'',
            'animation'=>'',
        ), $params ) );
        $args_recenposts = array(
                'posts_per_page'   => $number_of_products_by_auction,
                'orderby'          => 'post_date',
                'order'            => 'DESC',
                'post_type'        => 'product',
                'post_status'      => 'publish' 
                );
        $recentposts = get_posts($args_recenposts);
        $content  = "";
        $content .= '<div id="categoryid_'.$animation.'" class=" col-md-12 products_by_categories '.$animation.'">';

            // $content .= '<div class="testimonials_slider owl-carousel owl-theme animateIn" data-animate="'.$animation.'">';
                $content .= do_shortcode('[featured_products_labs per_page="'.$number_of_products_by_auction.'" columns="1" orderby="date" order="desc"]');
        // $shortcode_content .= '<div id="categoryid_'.$cat->term_id.'" class=" col-md-12 products_by_categories '.$cat->name.'">'.do_shortcode('[product_category columns="'.$number_of_columns.'" per_page="'.$number_of_products_by_category.'" category="'.$category.'"]').'</div>';
            // $content .= '</div>';
        $content .= '</div>';
        return $content;
}
add_shortcode('featured-products-slider', 'ibid_featured_shortcode');

   	/**
	 * featured_products shortcode
	 *
	 * @access public
	 * @param array $atts
	 * @return string
	 */
    function featured_products_labs( $atts ) {

        extract(shortcode_atts(array(
			'per_page' 	=> '12',
			'columns' 	=> '4',
			'orderby' => 'date',
			'order' => 'desc'
		), $atts));

		global $woocommerce_loop, $woocommerce;
        
        $meta_query  = $woocommerce->query->get_meta_query();  
        $args = array(
            'post_type'           => 'product',
            'post_status'         => 'publish',
            'ignore_sticky_posts' => 1,
            'posts_per_page'      => $per_page,
            'orderby'             => $orderby,
            'order'               => $order,
            'meta_query'          => $meta_query,
            'tax_query'           => array(array(
                'taxonomy' => 'product_visibility',
                'field'    => 'name',
                'terms'    => 'featured',
                'operator' => 'IN',
            )),
        );

		ob_start();

        $products = new WP_Query( $args );

		$woocommerce_loop['columns'] = $columns;

		if ( $products->have_posts() ) : ?>

			<?php woocommerce_product_loop_start(); ?>
			<div class="auctions_slider owl-carousel owl-theme animateIn" data-animate="<?=$animation ?>">

				<?php while ( $products->have_posts() ) : $products->the_post(); ?>

					<?php wc_get_template_part( 'content', 'product' ); ?>

				<?php endwhile; // end of the loop. ?>
			</div>

			<?php woocommerce_product_loop_end(); ?>
	   <?php else : ?>
            <?php wc_get_template( 'loop/no-products-found.php' ); ?>

		<?php endif;

		wp_reset_postdata();

		return '<div class="woocommerce">' . ob_get_clean() . '</div>';
    }
    add_shortcode( 'featured_products_labs', 'featured_products_labs' );


 #2.products on exchange

 function ibid_product_onexchange_with_xsthumbnails_shortcode( $params, $content ) {
    global $woocommerce_loop, $woocommerce;
    extract( shortcode_atts( 
        array(
            'number'                               => '',
            'number_of_products_by_category'       => '',
            'number_of_columns'                    => '',
            'button_text'                    => 'cick here to view',
            'products_label_text'                    => '',
            'category'                             => '',
            'overlay_color1'                       => '',
            'overlay_color2'                       => '',
            'bg_image'                       => '',
            'hide_empty'                           => '',
            'sale_text'                        =>''
        ), $params ) );

        $meta_query = $woocommerce->query->get_meta_query();

		$args = array(
			'post_type'	=> 'product',
			'post_status' => 'publish',
			'ignore_sticky_posts'	=> 1,
			'posts_per_page' => '12',
			'orderby' => 'date',
			'order' => 'desc',
			'meta_query' => $meta_query,
			'tax_query' => array(array('taxonomy' => 'product_type' , 'field' => 'slug', 'terms' => 'exchange')),
			'auction_arhive' => TRUE
		);

        ob_start();

        $count = 0;
        $products = new WP_Query( $args );?>
        <?php while ( $products->have_posts() ) : $products->the_post(); ?>

        <?php $count++;?>

        <?php endwhile; // end of the loop. ?>
    <?php 
    if (isset($bg_image) && !empty($bg_image)) {
        $bg_image = wp_get_attachment_image_src($bg_image, "full");
    }

    $category_style_bg = '';
    if (isset($bg_image) && !empty($bg_image)) {
        $category_style_bg .= 'background: url('.$bg_image[0].') no-repeat center center;';
    }else{
        $category_style_bg .= 'background: radial-gradient('.$overlay_color1.','.$overlay_color2.');';
    }

    // if ($button_text) {
    // 	$button_text_value = $button_text;
    // }else{
    // 	$button_text_value = 'View All Items';
    // }

    if ($products_label_text) {
    	$products_label_text_value = $products_label_text;
    }else{
    	$products_label_text_value = 'Products';
    }
    if(!$sale_text){
        $sale_text = 'ON EXCHANGE';
    }
// '.do_shortcode('[sale_products columns="'.$number_of_columns.'" per_page="'.$number_of_products_by_category.'"]').'

    // $cat = get_term_by('slug', $category, 'product_cat');
    $sales = wc_get_product_ids_on_sale();

    $shortcode_content = '';
    $shortcode_content .= '<div class="woocommerce_categories2">';
       
        $shortcode_content .= '<div class="products_category row">';
            $shortcode_content .= '<div class="category item col-md-3" >';
                $shortcode_content .= '<div style="'.$category_style_bg.'" class="category-wrapper">';
                    $shortcode_content .= '<a class="#categoryid">';
                        $shortcode_content .= '<span class="cat-name">'.$sale_text.'</span>';                    
                    $shortcode_content .= '</a>';
                    $shortcode_content .= '<br>'; 
                    $shortcode_content .= '<span class="cat-count"><strong>'. $count.'</strong> '.esc_html($products_label_text_value).'</span>';
                    $shortcode_content .= '<br>';
                    $shortcode_content .= '<div class="category-button">';
                    //    $shortcode_content .= '<a href="  '.get_site_url().'/product-category/'.$category.'  " class="button" title="View more" ><span>'.$button_text_value.'</span></a>';
                    $shortcode_content .= '</div>';
                $shortcode_content .= '</div>';    
            $shortcode_content .= '</div>';
                        $shortcode_content .= '<div id="categoryid" class=" col-md-9 products_by_categories ">'.do_shortcode('[recent_exchange_labs columns="'.$number_of_columns.'" per_page="'.$number_of_products_by_category.'"]').'
                        </div>';
        $shortcode_content .= '</div>';
    $shortcode_content .= '</div>';

    wp_reset_postdata();

    return $shortcode_content;
}
add_shortcode('exchange-product-categories-with-xsthumbnails', 'ibid_product_onexchange_with_xsthumbnails_shortcode');


 	/**
	 * recent_exchange shortcode
	 *
	 * @access public
	 * @param array $atts
	 * @return string
	 */
    function recent_exchange_labs( $atts ) {

		global $woocommerce_loop, $woocommerce;

		extract(shortcode_atts(array(
			'per_page' 	=> '12',
			'columns' 	=> '4',
			'orderby' => 'date',
			'order' => 'desc'
		), $atts));

		$meta_query = $woocommerce->query->get_meta_query();

		$args = array(
			'post_type'	=> 'product',
			'post_status' => 'publish',
			'ignore_sticky_posts'	=> 1,
			'posts_per_page' => $per_page,
			'orderby' => $orderby,
			'order' => $order,
			'meta_query' => $meta_query,
			'tax_query' => array(array('taxonomy' => 'product_type' , 'field' => 'slug', 'terms' => 'exchange')),
			// 'auction_arhive' => TRUE
		);

		ob_start();

		$products = new WP_Query( $args );

		$woocommerce_loop['columns'] = $columns;

		if ( $products->have_posts() ) : ?>

			<?php woocommerce_product_loop_start(); ?>

				<?php while ( $products->have_posts() ) : $products->the_post(); ?>

					<?php wc_get_template_part( 'content', 'product' ); ?>

				<?php endwhile; // end of the loop. ?>
			

			<?php woocommerce_product_loop_end(); ?>
	   <?php else : ?>
            <?php wc_get_template( 'loop/no-products-found.php' ); ?>

		<?php endif;

		wp_reset_postdata();

		return '<div class="woocommerce">' . ob_get_clean() . '</div>';
    }
    add_shortcode( 'recent_exchange_labs', 'recent_exchange_labs' );


