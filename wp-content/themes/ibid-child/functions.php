<?php

function ibid_child_scripts() {
    wp_enqueue_style( 'ibid-parent-style', get_template_directory_uri(). '/style.css' );
    wp_register_script( "maajik", get_stylesheet_directory_uri().'/js/maajik.js', array('jquery') );
    wp_localize_script( 'maajik', 'maAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));        
 
    wp_enqueue_script( 'jquery' );
    wp_enqueue_script( 'maajik' );
    wp_deregister_script('wc-checkout');
    // wp_enqueue_script('wc-checkout', get_template_directory_uri() . '/../storefront-child-theme-master/woocommerce/checkout.js', array('jquery', 'woocommerce', 'wc-country-select', 'wc-address-i18n'), null, true);

}
add_action( 'wp_enqueue_scripts', 'ibid_child_scripts' );

/*
 *  Remove vendor registration
 */

// function remove_vendor_registration(){
//     remove_action( 'woocommerce_register_form', 'dokan_seller_reg_form_fields' );
// }
// add_action( 'init', 'remove_vendor_registration' );


function user_login_verification(){

    // echo "hi it's working";
        $response = array();

        $creds = array();
        $creds['user_login'] = esc_attr($_POST['log']);
        $creds['user_password'] = esc_attr($_POST['pwd']);
        $creds['remember'] = true;
        // echo $creds['user_password'];

        $check = wp_authenticate_username_password( NULL, $creds['user_login'], $creds['user_password'] );
        if(is_wp_error( $check )){
            $response['type'] = "error";
            $response['error'] = $check->get_error_message(); 
        }
        else{
            $response['type'] = "success";
            wp_set_current_user( $id, $username );
            wp_set_auth_cookie( $id,true,true );
        }

        $result = json_encode($response);
        echo $result;
        die();

}

add_action("wp_ajax_user_login_verification", "user_login_verification");
add_action("wp_ajax_nopriv_user_login_verification", "user_login_verification");

function user_registration(){

    $errors = new WP_Error();
    
    $response = array();
    $response['count'] = 0;
    $billing_first_name = esc_attr($_POST['billing_first_name']);
   $billing_last_name = esc_attr($_POST['billing_last_name']);
    $email = esc_attr($_POST['email']);
    $password = esc_attr($_POST['password']);
    $username = esc_attr($_POST['username']);

    $WP_array = array (
        'user_login'    =>  $username,
        'user_email'    =>  $email,
        'user_pass'     =>  $password,
        'first_name'    =>  $billing_first_name,
        'last_name'     =>  $billing_last_name,
        'display_name' => $username
    ) ;

    $id = wp_insert_user( $WP_array ) ;
    if(is_wp_error( $id )){
        $response['type'] = "errors";
         $response['error'] = $id->get_error_message(); 
    }
    else{
        $response['type'] = "success";
        $response['success'] = "Successfully registered,verify your email to login";

        // wp_update_user( array ('ID' => $id, 'role' => 'customer') );
        // wp_set_current_user( $id, $username );
        // wp_set_auth_cookie( $id,true,true );
    }

    $result = json_encode($response);

    echo $result; 
    die();
    // echo "hello there";
}
add_action("wp_ajax_user_registration", "user_registration");
add_action("wp_ajax_nopriv_user_registration", "user_registration");


#--------------------------------------------------------------------
# CUSTOM SHORT CODES
#------------------------------------------------------------------

require_once get_stylesheet_directory() . '/inc/shortcodes/vc-shortcodes-labs.php';
require_once get_stylesheet_directory() . '/inc/shortcodes/shortcodes-labs.php';

#------------------------------------------------------------------
#change name of woocommerce checkout button
#----------------------------------------------------------------

add_action( 'woocommerce_order_button_text', 'custom_checkout_text' );
function custom_checkout_text() {
 return "Make Payments"; 
}


#----------------------------------------------------------------------------------------------
# adding custom product type to woocommerce
#----------------------------------------------------------------------------------
require_once get_stylesheet_directory() . '/inc/exchange/exchange-actions.php';

#-------------------------------------------------------------
# add metabox for product
#-------------------------------------------------------------

function wp_get_meta_box( $meta_boxes ) {
	// $prefix = 'prefix-';

	$meta_boxes[] = array(
		'id' => 'location',
		'title' => esc_html__( 'Location', 'metabox-online-generator' ),
		'post_types' => array('product' ),
		'context' => 'advanced',
		'priority' => 'default',
		'autosave' => 'false',
		'fields' => array(
			array(
				'id' => 'location',
				'type' => 'text',
				'name' => esc_html__( 'product_location', 'metabox-online-generator' ),
				'placeholder' => esc_html__( 'product location', 'metabox-online-generator' ),
			),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'wp_get_meta_box' );


#----------------------------------------------
#save product locations
#----------------------------------------------

add_action( 'save_post', 'save_post_data' );
function save_post_data($post_id)
{   
    $field = get_post_meta($post_id,'location');
    update_option('product_location', serialize($field));
         
}

#----------------------------------------------
# Remove product types
#----------------------------------------------

add_filter( 'product_type_selector', 'remove_product_types' );
 
function remove_product_types( $product_types ){
 
	// unset( $product_types['grouped'] );
	// unset( $product_types['external'] );
    // unset( $product_types['variable'] );
    
	return $product_types;
}

#------------------------------------------------
# rename product type
#-------------------------------------------------


add_filter( 'product_type_selector', 'rename_product_type' );
 
function rename_product_type( $product_types ){
 
	$product_types['simple'] = 'Sale';
	return $product_types;

}




// add_action( 'woocommerce_product_query', 'product_query' );

// function product_query( $q ){

//     $product_ids_on_sale = wc_get_product_ids_on_sale();

//     $q->set( 'post__in', $product_ids_on_sale );

// }

global $wp;

// $re = strcmp(get_site_url().$_SERVER['REQUEST_URI'],get_site_url().'/shop/sale-products');
// $re = !$re;
// echo strval(get_site_url().$_SERVER['REQUEST_URI']);
// echo strval(get_site_url().'/shop/sale-products');

// if( (strcmp(get_site_url().$_SERVER['REQUEST_URI'],get_site_url().'/shop/sale-products'))){
  
//     add_action( 'woocommerce_product_query', 'product_query' );

//     function product_query( $q ){
		

// 		$product_ids_on_sale = wc_get_product_ids_on_sale();

//         $q->set( 'post__in', $product_ids_on_sale );

//     }
// }







