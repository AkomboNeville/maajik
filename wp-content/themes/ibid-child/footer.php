<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package ibid
 */
?>
<?php
global $ibid_redux;
?>

    <?php if ( class_exists( 'ReduxFrameworkPlugin' ) ) { ?>
        <!-- BACK TO TOP BUTTON -->
        <a class="back-to-top modeltheme-is-visible modeltheme-fade-out" href="<?php echo esc_url('#0'); ?>">
            <span></span>
        </a>
    <?php }else{ ?>
        <?php if (ibid_redux('ibid_backtotop_status') == true) { ?>
            <!-- BACK TO TOP BUTTON -->
           <a class="back-to-top modeltheme-is-visible modeltheme-fade-out" href="<?php echo esc_url('#0'); ?>">
                <span></span>
            </a>
        <?php } ?>
    <?php } ?>

    <?php
        $footer_widgets = 'no-footer-widgets';
        if ( is_active_sidebar( 'footer_column_1' ) || is_active_sidebar( 'footer_column_2' ) || is_active_sidebar( 'footer_column_3' ) || is_active_sidebar( 'footer_column_4' ) || is_active_sidebar( 'footer_column_5' ) ) {
            $footer_widgets = 'has-footer-widgets';
        }
    ?>
    <!-- MAIN FOOTER -->
    <footer class="<?php echo esc_attr($footer_widgets); ?>">
        <?php if ( class_exists( 'ReduxFrameworkPlugin' ) || class_exists( 'WooCommerce' ) ) { ?>
            <?php if (ibid_redux('ibid-enable-footer-top') == true) { ?>
                <div class="top-footer row">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-1"></div>                       
                            <!-- <div class="col-md-8 left"><!?php esc_html_e( 'Browse through our products library!', 'ibid' ); ?></div> -->
                            <div class="col-md-10 ">
                                <form name="myform" method="GET" class="woocommerce-product-search menu-search" action="<?php echo esc_url(home_url('/')); ?>">
                                    <?php 
                                    if(isset($_REQUEST['product_cat']) && !empty($_REQUEST['product_cat'])) {
                                        $optsetlect=$_REQUEST['product_cat'];
                                    } else {
                                        $optsetlect=0;  
                                    }
                                    $args = array(
                                        'show_option_none' => esc_html__( 'Category', 'ibid' ),
                                        'option_none_value'  => '',
                                        'hierarchical' => 0,
                                        'class' => 'cat',
                                        'echo' => 1,
                                        'value_field' => 'slug',
                                        'hide_empty' => true,
                                        'selected' => $optsetlect
                                    );
                                    $args['taxonomy'] = 'product_cat';
                                    $args['name'] = 'product_cat';              
                                    $args['class'] = 'form-control1';
                                    
                                    // get all product types
                                    wp_dropdown_categories($args);
                                    $product_types = wc_get_product_types();
                                    // var_dump($product_types);
                                    ?>
                                    <select name="product_cat" id="product_cat" class="form-control1">
                                    <option value=""><?php esc_html_e('Available for', 'ibid'); ?></option>
                                    <?php foreach($product_types as $product_type ):?>
                                    <option class="level-0" value="<?= $product_type?>"><?=$product_type?></option>
                                    <?php
                                    endforeach     
                                    ?>
                                    </select>


                                    <input type="hidden" value="product" name="post_type">
                                    <input type="text"  name="s" class="search-field" maxlength="128" value="<?php echo esc_attr(get_search_query()); ?>" placeholder="<?php esc_attr_e('Search products...', 'ibid'); ?>">
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-search" aria-hidden="true"></i></button>
                                    <input type="hidden" name="post_type" value="product" />
                                </form>
                            </div>
                            <div class="col-md-1">
                                <!-- <form name="myform" method="GET" class="woocommerce-product-search menu-search" action="<?php echo esc_url(home_url('/')); ?>">
                                    <input type="hidden" value="product" name="post_type">
                                    <input type="text"  name="s" class="search-field" maxlength="128" value="<?php echo esc_attr(get_search_query()); ?>" placeholder="<?php esc_attr_e('Search products...', 'ibid'); ?>">
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-search" aria-hidden="true"></i></button>
                                    <input type="hidden" name="post_type" value="product" />
                                </form> -->
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        <?php } ?>

        <?php if ( class_exists( 'ReduxFrameworkPlugin' ) ) { ?>
            <?php if ( $ibid_redux['ibid-enable-footer-widgets'] ) { ?>
            <div class="container footer-top">
                <div class="row <?php echo esc_attr($ibid_redux['ibid_number_of_footer_columns']); ?>">

                    <?php
                    $columns    = 12/intval($ibid_redux['ibid_number_of_footer_columns']);
                    $nr         = array("1", "2", "3", "4", "6");

                    if (in_array($ibid_redux['ibid_number_of_footer_columns'], $nr)) {
                        $class = 'col-md-'.esc_html($columns);
                        for ( $i=1; $i <= intval( $ibid_redux['ibid_number_of_footer_columns'] ) ; $i++ ) { 

                            echo '<div class="'.esc_attr($class).' widget widget_text">';
                                dynamic_sidebar( 'footer_column_'.esc_html($i) );
                            echo '</div>';

                        }
                    }elseif($ibid_redux['ibid_number_of_footer_columns'] == 5){
                        #First
                        if ( is_active_sidebar( 'footer_column_1' ) ) {
                            echo '<div class="col-md-3 widget widget_text">';
                                dynamic_sidebar( 'footer_column_1' );
                            echo '</div>';
                        }
                        #Second
                        if ( is_active_sidebar( 'footer_column_2' ) ) {
                            echo '<div class="col-md-2 widget widget_text">';
                                dynamic_sidebar( 'footer_column_2' );
                            echo '</div>';
                        }
                        #Third
                        if ( is_active_sidebar( 'footer_column_3' ) ) {
                            echo '<div class="col-md-2 widget widget_text">';
                                dynamic_sidebar( 'footer_column_3' );
                            echo '</div>';
                        }
                        #Fourth
                        if ( is_active_sidebar( 'footer_column_4' ) ) {
                            echo '<div class="col-md-2 widget widget_text">';
                                dynamic_sidebar( 'footer_column_4' );
                            echo '</div>';
                        }
                        #Fifth
                        if ( is_active_sidebar( 'footer_column_5' ) ) {
                            echo '<div class="col-md-3 widget widget_text">';
                                dynamic_sidebar( 'footer_column_5' );
                            echo '</div>';
                        }
                    }
                    ?>
                </div>
            </div>
            <?php } ?>
        <?php } ?>

        <div class="footer">
            <div class="container">
                <div class="row">
                    <?php if ( class_exists( 'ReduxFrameworkPlugin' ) ) { ?>
                        <div class="col-md-6">
                            <p class="copyright"><?php echo wp_kses($ibid_redux['ibid_footer_text_left'], 'link'); ?></p>
                        </div>
                        <div class="col-md-6 payment-methods">
                            <p class="copyright"><?php echo wp_kses($ibid_redux['ibid_footer_text_right'], 'link'); ?></p>
                        </div>
                    <?php }else { ?>
                        <div class="col-md-6">
                            <p class="copyright"><?php esc_html_e( 'Copyright Maajik 2020.', 'ibid' ); ?></p>
                        </div>
                        <div class="col-md-6 payment-methods">
                            <p class="copyright"><?php esc_html_e( 'Cameroon biggest e-commerce platform', 'ibid' ); ?></p>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </footer>
</div>

<?php wp_footer(); ?>
</body>
</html>